"""
    Bimodal{P<:AbstractFloat} <: AbstractEnvironment

Implements a bandit problem with a bimodal reward function.

# Description
Bimodal implements a bandit problem with a bimodal reward function. The reward function
itself is a pdf of a mixture of Gaussians with a lower mode at -1.0 and a higher mode at
1.0. The modes have y values of 1.0 and 1.5 respectively. Both Gaussians have the
same standard deviation. Given some action, the reward consists of the pdf of this action
under this distribution.

This environment is completely deterministic.
# State and Observations
Since this is a bandit problem, there is no concept of state. Even so, function
approximators need some kind of input, therefore the only "state observation" is `[1]`, a
vector with a single element, which is `1`.

# Actions
Actions are bounded between `[-2, 2]`.

# Goal/Rewards
Let `N(a; μ, σ)` be the normal density function evaluated at `a` for a normal distribution
with mean `μ` and standard deviation `σ`. Then, the reward function `r(a)` is given by:

    r(a) = 1.0 * N(a; -1.0, 0.2) + 1.5 * N(a; 1.0, 0.2)
"""
mutable struct Bimodal{
        P<:AbstractFloat,
        A<:AbstractSpace,
        O<:AbstractSpace,
} <: AbstractEnvironment
    _obsspace::O
    _actionspace::A

    _reward::P
    _state::Vector{P}
    _done::Bool
    _γ::P

    # y values for modes of reward function
    _mode1::P
    _mode2::P

    # Standard deviation of reward function
    _stddev::P

    function Bimodal{P}() where {P<:AbstractFloat}
        high_obs = 1.0f0
        low_obs = high_obs
        obsspace = Box{P}([low_obs], [high_obs])
        state = [high_obs]

        high_action = 2.0f0
        low_action = -high_action
        actionspace = Box{P}([low_action], [high_action])

        γ = 0f0 # Bandit problem

        O = typeof(obsspace)
        A = typeof(actionspace)

        return new{P,A,O}(obsspace, actionspace, 0f0, state, false, γ, 1f0, 1.5f0, 0.2f0)
    end
end

Bimodal() = Bimodal{Float32}()

function RLCore.reset!(b::Bimodal{P})::Vector{P} where {P<:AbstractFloat}
    b._done = false

    return b._state
end

function RLCore.envstep!(b::Bimodal{P}, action)::Tuple{Vector{P},P,Bool,P} where
{P<:AbstractFloat}
    b._reward = _reward(b, action)
    b._done = true

    return b._state, b._reward, true, γ(b)
end

function RLCore.reward(b::Bimodal{P})::P where {P<:AbstractFloat}
    return b._reward
end

function RLCore.isterminal(b::Bimodal)::Bool
    return b._done
end

function RLCore.γ(b::Bimodal{P})::P where {P<:AbstractFloat}
    return b._γ
end

function RLCore.observation_space(b::Bimodal{P})::AbstractSpace{P,1} where
{P<:AbstractFloat}
    return b._obsspace
end

function RLCore.action_space(b::Bimodal)::AbstractSpace
    return b._actionspace
end

function Base.show(io::IO, ::Bimodal)
    print(io, "Bimodal")
    return nothing
end

"""
    _reward(b::Bimodal, action)

Computes the reward for taking action `action` in environment `b`.
"""
function _reward(b::Bimodal{P}, action)::P where {P<:AbstractFloat}
    action_range = high(action_space(b)) .- low(action_space(b))
    step = action_range / 4

    maxima1 = low(action_space(b)) .+ step
    maxima2 = maxima1 .+ 2 .* step

    modal1 = b._mode1 * exp.(-0.5 * ((action .- maxima1) / b._stddev).^2.0)
    modal2 = b._mode2 * exp.(-0.5 * ((action .- maxima2) / b._stddev).^2.0)

    reward =  modal1 .+ modal2

    if ndims(reward) != 1 || size(reward)[1] != 1
        error("non-scalar reward computed")
    end
    return reward[1]
end

