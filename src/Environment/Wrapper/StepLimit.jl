"""
    StepLimit <: AbstractEnvironmentWrapper

Limit the steps taken per episode in an environment
"""
mutable struct StepLimit <: AbstractEnvironmentWrapper
    env::AbstractEnvironment
    episode_steps::Integer
    steps_per_episode::Integer

    function StepLimit(env::AbstractEnvironment, steps::Integer)
        return new(env, 0, steps)
    end
end

function StepLimit(env::AbstractEnvironment; steps::Integer)
    StepLimit(env, steps)
end

function RLCore.reset!(t::StepLimit)::AbstractArray
    t.episode_steps = zero(t.episode_steps)
    reset!(wrapped(t))
end

function RLCore.envstep!(t::StepLimit, action)
    t.episode_steps += 1
    state, reward, done, discount = step!(wrapped(t), action)

    done = done || t.episode_steps >= t.steps_per_episode
    return state, reward, done, discount
end

function RLCore.reward(t::StepLimit)::AbstractFloat
    return reward(wrapped(t))
end

function RLCore.action_space(t::StepLimit)::AbstractSpace
    action_space(wrapped(t))
end

function RLCore.observation_space(t::StepLimit)::AbstractSpace
    observation_space(wrapped(t))
end

function RLCore.isterminal(t::StepLimit)::Bool
    isterminal(wrapped(t)) || t.episode_steps >= t.steps_per_episode
end

function RLCore.γ(t::StepLimit)
    γ(wrapped(t))
end

function RLCore.wrapped(t::StepLimit)::AbstractEnvironment
    return t.env
end

function Base.show(io::IO, t::StepLimit)
    print(io, "StepLimit($(t.env); steps=$(t.steps_per_episode))")
end
