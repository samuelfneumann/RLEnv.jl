"""
    Pendulum{P<:AbstractFloat} <: AbstractEnvironment

# Description
The inverted pendulum swingup problem is based on the classic problem in
control theory. The system consists of a pendulum attached at one end to a
fixed point, and the other end being free. The pendulum starts facing
straight downwards, an angle of ±π. The goal is to apply torque on the
free end to swing it into an upright position, an angle of 0, with its
centre of gravity right above the fixed point.

## Observation Space
If `trig_features == True`, then the observation is an `ndarray` with shape
`(3,)` representing the x-y coordinates of the pendulum's free end and its
angular velocity:
| Num | Observation  | Min  | Max |
|-----|--------------|------|-----|
| 0   | x = cos(θ)   | -1.0 | 1.0 |
| 1   | y = sin(θ)   | -1.0 | 1.0 |
| 2   | θ̇            | -8.0 | 8.0 |

If `trig_features == False`, then the observation is an `ndarray` with
shape `(2,)` representing the pendulum's angle and angular velocity:
| Num | Observation | Min  | Max |
|-----|-------------|------|-----|
| 0   | θ           | -π   | π   |
| 1   | θ̇           | -8.0 | 8.0 |

The angle of the pendulum is θ and is normalized/wrapped to stay
in [-π, π).

## Action Space
In the continuous-action setting, the action is a `ndarray` with shape
`(1,)` representing the torque applied to free end of the pendulum.
| Num | Action | Min  | Max |
|-----|--------|------|-----|
| 0   | Torque | -2.0 | 2.0 |

In the discrete-action setting, the action is an integer in the set
`{0, 1, 2}`, corresponding to maximum torque in the negative direction,
no torque, maximum torque in the positive direction.

## Rewards
The reward function is defined as cos(θ) when `sparse_rewards == False`.
Otherwise, the reward is +1 if |θ| < π/12 and 0 otherwise.

## Starting State
The starting state is facing straight down with no velcoity: [-π, 0].

## Episode termination
This environment is continuing and does not have a termination.
"""
mutable struct Pendulum{
        P<:AbstractFloat,
        A<:AbstractSpace,
        O<:AbstractSpace,
        R<:AbstractRNG,
} <: AbstractEnvironment
    _observationspace::O
    _actionspace::A
    _γ::P
    _sparse_rewards::Bool

    _max_speed::P
    _max_torque::P
    _dt::P

    _mass::P
    _length::P
    _gravity::P

    _rng::R

    _state::Vector{P}
    _trig_features::Bool

    function Pendulum{P}(
        rng::R,
        continuous,
        sparse_rewards,
        trig_features,
        γ,
    ) where {P<:AbstractFloat,R<:AbstractRNG}
        max_speed = 8.0
        max_torque = 2.0
        dt = 0.05
        mass = 1.0
        length = 1.0
        gravity = 9.8

        if !trig_features
            # Encode states as [θ, θ̇]
            high = [π, max_speed]
            low = -high
        else
            # Encode states as [cos(θ), sin(θ), θ̇]
            high = [1, 1, max_speed]
            low = -high
        end

        obs_space = Box{P}(low, high)
        action_space = continuous ? Box{P}(-max_torque, max_torque) : Discrete(3)
        O = typeof(obs_space)
        A = typeof(action_space)

        p = new{P,A,O,R}(
            obs_space,
            action_space,
            γ,
            sparse_rewards,
            max_speed,
            max_torque,
            dt,
            mass,
            length,
            gravity,
            rng,
            [-π , 0],
            trig_features,
        )

        reset!(p)
        return p
    end
end

function Pendulum{P}(
    rng::AbstractRNG;
    continuous,
    γ = 0.99f0,
    trig_features = false,
    sparse_rewards = false,
) where {P<:AbstractFloat}
    return Pendulum{P}(
        rng,
        continuous,
        sparse_rewards,
        trig_features,
        γ,
    )
end

function Pendulum(
    rng::AbstractRNG;
    continuous,
    γ::P = 0.99f0,
    trig_features = false,
    sparse_rewards = false,
) where {P<:AbstractFloat}
    return Pendulum{P}(
        rng,
        continuous,
        sparse_rewards,
        trig_features,
        γ,
    )
end


function RLCore.reset!(p::Pendulum{P})::Vector{P} where {P<:AbstractFloat}
    return p._state = [-π, 0.0,]
end

function RLCore.envstep!(
    p::Pendulum{P},
    action,
)::Tuple{Vector{P},P,Bool,P} where {P<:AbstractFloat}
    # Sanitize the input action depending on whether we are in the
    # discrete or continuous setting
    if !(p |> action_space |> continuous)
        u = _discrete_action(p, action)
    else
        u = _continuous_action(p, action)
    end

    θ, dθ = p._state
    costs = _angle_normalize(θ)^2 + 0.1 * dθ^2 + 0.001 * u^2

    new_dθ = 3.0f0 * p._gravity / (2.0f0 * p._length) * sin(θ)
    new_dθ += 3.0f0 / (p._mass * p._length^2.0f0) * u
    new_dθ *= p._dt
    new_dθ += dθ
    new_θ = θ + new_dθ * p._dt

    p._state = [new_θ, new_dθ]

    return _get_obs(p), reward(p), isterminal(p), γ(p)
end

function _get_obs(p::Pendulum{P})::Vector{P} where {P<:AbstractFloat}
    if p._trig_features
        θ, θ̇ = p._state
        return [cos(θ), sin(θ), θ̇]
    else
        return p._state
    end
end

function RLCore.reward(p::Pendulum{P})::P where {P<:AbstractFloat}
    if p._sparse_rewards
        return  abs(p._state[1]) < π/12 ? 1 : 0
    else
        return cos(p._state[1])
    end
end

function RLCore.isterminal(p::Pendulum)::Bool
    return false
end

function RLCore.observation_space(p::Pendulum{P})::AbstractSpace{P,1} where
{P<:AbstractFloat}
    p._observationspace
end

function RLCore.action_space(p::Pendulum)
    p._actionspace
end

function RLCore.γ(p::Pendulum{P})::P where {P<:AbstractFloat}
    return p._γ
end

function _angle_normalize(θ)
    return ((θ + π) % (2π)) - π
end

function Base.show(io::IO, p::Pendulum)
    print(io, "Pendulum")
end

function _discrete_action(p::Pendulum, action)
    # Ensure the action is in the set of available actions
    lower = p |> action_space |> low
    upper = p |> action_space |> high

    action = action[1]
    return p._max_torque * (action - 2)
end

function _continuous_action(p::Pendulum{P}, action)::P where {P<:AbstractFloat}
    lower = p |> action_space |> low
    upper = p |> action_space |> high

    return action[1]
end

