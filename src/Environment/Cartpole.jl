@enum Integrator euler = 1 semi_implicit = 2

"""
    Cartpole{P<:AbstractFloat} <: AbstractEnvironment


The classic control Cartpole environment.

# Description
In this environment, a pole is attached to a cart. The cart starts with the pole nearly
upright, and the goal is to keep the pole in an upright position for as long as possible.

# State and Observations
The state observations consists of the `x` position of the cart, the cart's velocity, and
angle of the pole with respect to the cart, and the angular velocity of the pole. The
position of the cart is bounded between the `± x_threshold` constructor argument; the
velocity of the cart is bounded between the `± max_speed` argument; the angle of the pole is
ounded between `±theta_threshold_radians`;
and the angular velocity of the pole is between the `± max_angular_velocity` argument.

The episode is over when the pole angle falls below the `±theta_threshold_radians`
constructor argument.

# Actions
Actions may be discrete or continuous. If actions are discrete, then
3 actions are available:

Action | Meaning
-------|------------------------
   1   | Maximum acceleration of the cart leftwards
   2   | Nothing
   3   | Maximum acceleration of the cart rightward

If actions are continuous, then they are between
`[-max_action, max_action]`, where `-max_action` is maximum acceleration to the
left and `max_action` is maximum acceleration to the right; `max_accel` is given as a
constructor argument.

Given an action of `a ∈ [-max_action, max_action]` is taken, the effective action is `a *
force_mag`, where `force_mag` is the force magnifier, given as an argument to the
constructor.

# Goal/Rewards
The goal of the agent is to keep the pole within the angles `±theta_threshold_radians` as
long as possible, and the rewards are `1` on each timestep until the terminal timestep, at
which the reward is -1.

"""
mutable struct Cartpole{
        P<:AbstractFloat,
        A<:AbstractSpace,
        O<:AbstractSpace,
        R<:AbstractRNG,
} <: AbstractEnvironment
    _gravity::P
    _cartmass::P
    _polemass::P
    _length::P # Half the pole's length
    _force_mag::P
    _τ::P # Seconds between state updates
    _integrator::Integrator

    # Threshold at which to end an episode
    _theta_threshold_radians::P # Max/min angles of the pole
    _x_threshold::P # Max/min x position of the cart
    _max_speed::P # Max/min speed of the cart
    _max_angular_velocity::P # Max/min angular velocity of the pole

    _obsspace::O
    _actionspace::A
    _continuous::Bool # Whether actions should be continuous or not

    _state::Vector{P}
    _γ::P
    _reward::P
    _rng::R
    _sparse_rewards::Bool

    function Cartpole{P}(
        rng::R,
        continuous,
        γ,
        sparse_rewards,
    ) where {P,R}
        gravity = 9.8
        cartmass = 1.0
        polemass = 0.1
        length = 0.5
        force_mag = 10.0
        tau = 0.02
        integrator = euler
        theta_threshold_radians = 24 * π / 360
        x_threshold = 2.4
        max_speed = 2.0
        max_angular_velocity = 2.0
        max_action = 1.0

        high = [2x_threshold, max_speed, 2theta_threshold_radians, max_angular_velocity]
        low = -high
        obs_space = Box{P}(low, high)
        action_space = continuous ? Box{P}(-max_action, max_action) : Discrete(3)

        O = typeof(obs_space)
        A = typeof(action_space)

        c = new{P,A,O,R}(
            gravity,
            cartmass,
            polemass,
            length,
            force_mag,
            tau,
            integrator,
            theta_threshold_radians,
            x_threshold,
            max_speed,
            max_angular_velocity,
            obs_space,
            action_space,
            continuous,
            P[0f0, 0f0],
            γ,
            0.0,
            rng,
            sparse_rewards,
        )
        reset!(c)
        return c
    end
end

function Cartpole{P}(
    rng::AbstractRNG;
    continuous,
    γ = 1f0,
    sparse_rewards = false,
) where {P<:AbstractFloat}
    return Cartpole{P}(rng, continuous, γ, sparse_rewards)
end

function Cartpole(
    rng::AbstractRNG;
    continuous,
    γ::P = 1f0,
    sparse_rewards = false,
) where {P<:AbstractFloat}
    return Cartpole{P}(rng, continuous, γ, sparse_rewards)
end


function RLCore.reset!(c::Cartpole{P})::Vector{P} where {P<:AbstractFloat}
    # Random starting state in [-0.05, 0.05]^4
    c._state = rand(c._rng, Float32, 4) .* (0.1) .- 0.05

	 return c._state
end

function RLCore.envstep!(
    c::Cartpole,
    action,
)
    # Convert action
    if !c._continuous
        a = _discrete_action(c, action)
    else
        a = _continuous_action(c, action)
    end

    # Magnify the force, which is already in [-1, 1]
    force = c._force_mag * a

    x, ẋ, θ, θ̇ = c._state

    cosθ = cos(θ)
    sinθ = sin(θ)

    temp = (force + _polemass_length(c) * θ̇^2 * sinθ) / _total_mass(c)
    θacc =
        (c._gravity * sinθ - cosθ * temp) /
        (c._length * (4.0 / 3.0 - c._polemass * cosθ^2 / _total_mass(c)))
    xacc = temp - _polemass_length(c) * θacc * cosθ / _total_mass(c)

    # Transition to the next state
    if c._integrator == euler
        x += (c._τ * ẋ)
        ẋ += (c._τ * xacc)
        θ += (c._τ * θ̇)
        θ̇ == (c._τ * θacc)
    elseif c._integrator == semi_implicit
        ẋ += (c._τ * xacc)
        x += (c._τ * ẋ)
        θ̇ == (c._τ * θacc)
        θ += (c._τ * θ̇)
    else
        error("no such integrator $c._integrator")
    end

    c._state = [x, ẋ, θ, θ̇]
    done = isterminal(c)
    c._reward = done ? -1 : 1

    return c._state, c._reward, done, γ(c)
end

function RLCore.isterminal(c::Cartpole)::Bool
    x, _, θ, _ = c._state
    return x < -c._x_threshold ||
           x > c._x_threshold ||
           θ > c._theta_threshold_radians ||
           θ < -c._theta_threshold_radians
end

function RLCore.γ(c::Cartpole{P})::P where {P<:AbstractFloat}
    return isterminal(c) ? 0.0 : c._γ
end

function RLCore.observation_space(c::Cartpole{P})::AbstractSpace{P,1} where
{P<:AbstractFloat}
    return c._obsspace
end

function RLCore.action_space(c::Cartpole)::AbstractSpace
    return c._actionspace
end

function RLCore.reward(c::Cartpole{P})::P where {P<:AbstractFloat}
    if c._sparse_rewards
        return isterminal(c) ? -1 : 0
    else
        return c._reward
    end
end

function Base.show(io::IO, c::Cartpole)
    type = continuous(action_space(c)) ? "Continuous" : "Discrete"
    print(io, "Cartpole")
end

"""
    _polemass_length(c::Cartpole)::Float32

Return the linear density of the pole
"""
function _polemass_length(c::Cartpole{P})::P where {P<:AbstractFloat}
    return c._polemass * c._length
end

"""
    _total_mass(c::Cartpole)::Float32

Return the total mass of the cartpole
"""
function _total_mass(c::Cartpole{P})::P where {P<:AbstractFloat}
    return c._polemass + c._cartmass
end

function _discrete_action(::Cartpole, a)::Int
    return a[1] - 2 # Convert to be in (-1, 0, 1)
end

function _continuous_action(c::Cartpole{P}, a)::P where {P<:AbstractFloat}
    lower = c |> action_space |> low
    upper = c |> action_space |> high
    return a[1]
end

