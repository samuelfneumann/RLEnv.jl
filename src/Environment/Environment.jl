"""
    Environment

Environments for agents to interact with
"""
module Environment

export MountainCar, Cartpole, Pendulum, Bimodal, Gridworld, Acrobot

using RLCore
using Random
using ..RLEnv
using ..Spaces

include("Cartpole.jl")
include("MountainCar.jl")
include("Acrobot.jl")
include("Pendulum.jl")
include("Gridworld.jl")
include("Bimodal.jl")

"""
    Wrapper

Wrappers for environments that change the behaviour of environments
"""
module Wrapper
export StepLimit, ClipAction, Discretize, TransformReward, CastAction, CastObservation
export AbstractWrapper, AbstractActionWrapper, AbstractRewardWrapper
export AbstractObservationWrapper

using RLCore
using ..Environment
using ...Spaces

include("Wrapper/ClipAction.jl")
include("Wrapper/CastAction.jl")
include("Wrapper/CastObservation.jl")
include("Wrapper/Discretize.jl")
include("Wrapper/StepLimit.jl")
include("Wrapper/TransformReward.jl")
end
end
