"""
N-dimensional discrete set of points of type T, where each dimension
is between `[0⃗, n⃗]`. `n⃗` itself is N-dimensional.
"""
struct Discrete{T<:Integer,N} <: AbstractSpace{T,N}
    shape::NTuple{N,Int}
    n::Array{T,N}

    function Discrete{T,N}(shape::NTuple{N,Int},
        n::AbstractArray{T,N}) where {T<:Integer,N}

        N isa Integer || throw(TypeError(:Box, "N must be an Integer", Integer,
            typeof(N)))

        new(shape, n)
    end
end

function Discrete{T}(n) where {T<:Integer}
    ndim = max(1, ndims(n))
    if n isa Integer
        size = (1,)
        n = [n]
    else
        size = size(n)
    end

    return Discrete{T,ndim}(size, convert.(T, n))
end

function Discrete(n::AbstractArray{T,N}) where {T<:Integer,N}
    Discrete{T,N}(size(n), n)
end

function Discrete(n::T) where {T<:Integer}
    Discrete{T,1}((1,), [n])
end

Base.ndims(d::Discrete{T,N}) where {T,N} = N

function Base.rand(
        rng::AbstractRNG,
        d::Discrete{T,N},
        dims::NTuple{M,Int},
        keepdims=false,
) where {T<:Number,N,M}
    return rand(rng, b, dims...; keepdims = keepdims)
end

function Base.rand(
        rng::AbstractRNG,
        d::Discrete{T, N},
        dims::Int...;
        keepdims=false,
) where {T<:Integer,N}
    # Squeeze dimensions of size 1 if appropriate
    if !keepdims
        dims = Int[dims...]
        dims = filter!(elem->elem!=1, dims)
    end

    # Reshape for broadcasting
    high_d = reshape(high(d), (size(d)..., 1))
    low_d = reshape(low(d), (size(d)..., 1))

    # Generate unbounded positive random numbers of type T
    u = rand(rng, T, prod((dims..., size(d)...)))
    u = abs.(u)

    # Reshape to requested size and bound each dimension to be between [low(d), high(d)]
    u = reshape(u, size(d)..., dims...)
    u .%= (high_d - low_d) .+ low_d
    return u
end

function RLCore.high(d::Discrete{T,N})::AbstractArray{T,N} where {T<:Integer,N}
    d.n
end

function RLCore.low(d::Discrete{T,N})::AbstractArray{T,N} where {T<:Integer,N}
    ones(T, d.shape)
end

function RLCore.bounded(d::Discrete{T,N})::AbstractArray{Bool,N} where {T<:Integer,N}
    d.n .< Inf
end

function RLCore.continuous(::Discrete)::Bool
    return false
end

function RLCore.discrete(::Discrete)::Bool
    return true
end

function Base.contains(d::Discrete{T,N}, point::AbstractArray{T,N}) where {T<:Integer,N}
    return size(d) == size(point) && all(low(d) .<= point .<= high(d))
end

function Base.contains(d::Discrete{T,1}, point::T) where {T<:Integer}
    return all(low(d) .<= point .<= high(d))
end

function Base.size(d::Discrete)::Tuple
    return d.shape
end

function Base.show(io::IO, d::Discrete)
	print(io, "Discrete($(d.n...))")
end
