"""
    Box{T<:Real, N} <: AbstractSpace{T,N}

n-dimensional set of points of type T between `[l⃗, h⃗]`, where both `l⃗`
and `h⃗` are N-dimensional. The set may be discrete or continuous,
depening on if the `Box` is parameterized with `Integer`s or
`AbstractFloat`s.
"""
struct Box{T<:Real,N} <: AbstractSpace{T,N}
    shape::NTuple{N,Int}
    low::AbstractArray{T,N}
    high::AbstractArray{T,N}

    function Box(shape::NTuple{N,Int}, low::AbstractArray{T,N},
        high::AbstractArray{T,N}) where {T<:Real,N}
        N isa Integer || throw(TypeError(:Box, "N must be an Integer", Integer,
            typeof(N)))

        maximum(low .> high) && error("cannot create Box space with low > high")

        new{T,N}(shape, low, high)
    end
end

function Box{T}(low::Vector, high::Vector) where {T<:Real}
    if length(low) != length(high)
        l = length(low)
        h = length(high)
        error("low and high must have same number of elements but got $l and $h")
    end

    low = convert.(T, low)
    high = convert.(T, high)

    return Box(size(low), low, high)
end

function Box(low::Vector, high::Vector)
    if length(low) != length(high)
        l = length(low)
        h = length(high)
        error("low and high must have same number of elements but got $l and $h")
    end

    low, high = promote(low, high)
    return Box(size(low), low, high)
end

function Box(low::Vector{T}, high::Vector{T}) where {T<:Real}
    if length(low) != length(high)
        l = length(low)
        h = length(high)
        error("low and high must have same number of elements but got $l and $h")
    end
    return Box(size(low), low, high)
end

function Box{T}(low, high) where {T<:Real}
    low = convert(T, low)
    high = convert(T, high)
    return Box((1,), [low], [high])
end

function Box(low::T, high::T) where {T<:Real}
    return Box((1,), [low], [high])
end

Base.ndims(b::Box{T,N}) where {T,N} = N

function Base.rand(
        rng::AbstractRNG,
        b::Box{T,N},
        dims::NTuple{M,Int},
        keepdims=false,
) where {T<:Number,N,M}
    return rand(rng, b, dims...; keepdims = keepdims)
end

function Base.rand(
        rng::AbstractRNG,
        b::Box{T,N},
        dims::Int...;
        keepdims=false,
) where {T<:Number,N}
    # Squeeze dimensions of size 1 if appropriate
    if !keepdims
        dims = Int[dims...]
        dims = filter!(elem->elem!=1, dims)
    end

    # Reshape for broadcasting
    high_b = reshape(high(b), (size(b)..., 1))
    low_b = reshape(low(b), (size(b)..., 1))

    u = rand(rng, T, size(b)..., prod(dims))
    u = u .* (high_b - low_b) .+ low(b)

    # Reshape to dims batch size
    return reshape(u, (size(b)..., dims...))
end

function RLCore.bounded(b::Box{T,N})::AbstractArray{Bool,N} where
{T<:Real,N}
    below = low(b) .> -Inf
    above = high(b) .< Inf
    below .& above
end

function RLCore.high(b::Box{T,N})::AbstractArray{T,N} where {T,N}
    b.high
end

function RLCore.low(b::Box{T,N})::AbstractArray{T,N} where {T,N}
    b.low
end

RLCore.continuous(::Box)::Bool = true
RLCore.discrete(::Box)::Bool = false

function Base.contains(b::Box{T,N}, point::AbstractArray{T,N}) where {T<:Real,N}
    return size(point) == size(b) && all(low(b) .<= point .<= high(b))
end

function Base.eltype(b::Box{T,N}) where{T,N}
    return T
end

function Base.in(point, b::Box)
    return contains(b, point)
end

function Base.size(b::Box)::Tuple
    return b.shape
end

function Base.show(io::IO, b::Box)
    l = low(b)
    h = high(b)
    print(io, "Box(low = $l, high = $h)")
end

