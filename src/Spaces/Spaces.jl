module Spaces

using ..RLEnv
using RLCore
using Random

export Box, Discrete

include("Box.jl")
include("Discrete.jl")

end
