module RLEnv

using Random
using Logging
using RLCore

include("Spaces/Spaces.jl")
include("Environment/Environment.jl")

using .Environment
using .Environment.Wrapper
using .Spaces

# rng = Xoshiro(1)

# env = Gridworld(1; rows=3, cols=3, use_floating_point_obs=false)
# env = Pendulum(rng; continuous=true)
# env = ClipAction(env)
# env = CastObservation{UInt}(env)
# # println(typeof(high(observation_space(env))))
# state = reset!(env)
# println(reshape(state, 3, 3))
# state, r, done, γ = step!(env, [3])
# println(reshape(state, 3, 3), " ", γ, " ", done, " ", r)
# state, r, done, γ = step!(env, [3])
# println(reshape(state, 3, 3), " ", γ, " ", done, " ", r)
# state, r, done, γ  = step!(env, [2])
# println(reshape(state, 3, 3), " ", γ, " ", done, " ", r)
# state, r, done, γ  = step!(env, [2])
# println(reshape(state, 3, 3), " ", γ, " ", done, " ", r)

# env = Pendulum{Float32}(1; continuous=true, sparse_rewards=false)
# rand(action_space(env))
# env = CastAction(StepLimit(ClipAction(env), 1000))
# println(reset!(env))
# println(step!(env, [1]))
# println()
# println(eltype(action_space(env)))
# println(ndims(action_space(env)))
# println()
# println(eltype(observation_space(env)))
# println(ndims(observation_space(env)))
# println(env)

# rewards = 0
# done = false
# st = reset!(env)
# while !done
#     global st, rewards, done, env
#     ac = st[2] < 0 ? [-1] : [1]
#     st, r, done, γ = step!(env, ac)
#     rewards += r
# end

# print(rewards)

end
