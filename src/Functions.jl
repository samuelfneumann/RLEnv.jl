"""
    iscallable(f)

Return whether `f` is callable
"""
iscallable(f) = !isempty(methods(f))
