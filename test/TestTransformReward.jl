@testset "TestTransformReward" begin
    for env_type in [MountainCar, Pendulum, Cartpole]
        T = Random.rand([Float64, Float32])
        env = TransformReward(env_type{T}(1), x -> x - 1.0)

        # Ensure the observation type is left unaltered
        @test reset!(env)[1] isa T

        # Ensure the types of outputs of step! are unaltered
        s, r, done, γ = step!(env, 1)
        @test s[1] isa T
        @test r isa T
        @test done isa Bool
        @test γ isa T

        # Ensure rewards were altered
        @test isapprox(reward(wrapped(env)), r + 1.0)
        @test isapprox(reward(env), r)

        @test reset!(env)[2] isa T
    end
end
