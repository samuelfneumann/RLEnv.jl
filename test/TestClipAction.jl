@testset "TestClipAction" begin
    T = Random.rand([Float64, Float32])
    env_type = Random.rand([MountainCar, Pendulum, Cartpole])
    env = ClipAction(env_type{T}(1; continuous=true))

    @testset "ReturnUnchanged" begin
        @test reset!(env)[1] isa T

        s, r, done, γ = step!(env, [1])
        @test s[1] isa T
        @test r isa T
        @test done isa Bool
        @test γ isa T

        @test reset!(env)[2] isa T
    end

    @testset "DontChangeActionType" begin
        println(T)
        @test action(env, [convert(T, 1.0)]) isa Vector{T}
    end

    @testset "AllowActionsOutsideRanges" begin
        action = high(action_space(env)) .+= 1
        step!(env, action)

        action = low(action_space(env)) .+= 1
        step!(env, action)
    end
end
