@testset "TestDiscretize" begin
    T = Random.rand([Float64, Float32])
    for env_type in [MountainCar, Pendulum, Cartpole]
        env = Discretize(env_type{T}(1; continuous=true), 3)

        # Ensure the returned values for reset! are unchanged
        @test reset!(env)[1] isa T

        # Ensure the returned values for step! are unchanged
        s, r, done, γ = step!(env, 1)
        @test s[1] isa T
        @test r isa T
        @test done isa Bool
        @test γ isa T

        # Enure proper errors are thrown
        @test_throws DomainError step!(env, 10)
        @test_throws DomainError step!(env, 1f0)

        # Check to make sure that there are 3 available actions
        @test action_space(env) isa Discrete
        @test high(action_space(env))[1] == 3
    end
end
