include("../src/RLEnv.jl")

using Main.RLEnv
using Main.RLEnv
using Main.RLEnv
using Main.RLEnv.Spaces
using Main.RLEnv.Environment
using Main.RLEnv.Environment.Wrappers
using Test
using Random
using Dates

Random.seed!(trunc(UInt, Dates.datetime2unix(now())))

@testset "Tests" begin
    include("./TestClipAction.jl")
    include("./TestDiscretize.jl")
    include("./TestStepLimit.jl")
    include("./TestTransformReward.jl")
end

exit()
