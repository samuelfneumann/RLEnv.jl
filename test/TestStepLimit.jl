@testset "TestStepLimit" begin
    @testset "StepLimitRespected" begin
        T = Random.rand([Float64, Float32])
        for env_type in [MountainCar, Pendulum, Cartpole]
            env = StepLimit(env_type{T}(1; continuous=false); steps=2)
            reset!(env)
            step!(env, 1)
            _, _, done, _ = step!(env, 1)

            @test done
        end
    end

    @testset "ReturnUnchanged" begin
        T = Random.rand([Float64, Float32])
        for env_type in [MountainCar, Pendulum, Cartpole]
            env = StepLimit(env_type{T}(1); steps=1000)
            @test reset!(env)[1] isa T

            s, r, done, γ = step!(env, 1)
            @test s[1] isa T
            @test r isa T
            @test done isa Bool
            @test γ isa T

            @test reset!(env)[2] isa T
        end
    end
end
